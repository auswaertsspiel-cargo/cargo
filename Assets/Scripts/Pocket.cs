using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocket : MonoBehaviour
{
    public float attractionForce = 0.03f;  

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            Rigidbody ballRigidbody = other.gameObject.GetComponent<Rigidbody>();
            other.gameObject.layer = 7;
            Vector3 forceDirection = transform.position - other.transform.position;
            ballRigidbody.AddForce(forceDirection.normalized * attractionForce, ForceMode.Impulse);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            Rigidbody ballRigidbody = other.gameObject.GetComponent<Rigidbody>();
            ballRigidbody.useGravity = true;
            other.gameObject.layer = 0;
        }
    }
}
