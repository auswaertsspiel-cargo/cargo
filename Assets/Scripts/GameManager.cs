using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject cueBall;
    public GameObject billardBall;
    public List<Texture2D> ballTextures;

    private void Update()
    {
        OVRInput.Update();
        if (OVRInput.Get(OVRInput.Button.One))
        {
            Debug.Log("Button pressed");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void InstantiateBall(Vector3 position, Texture2D texture)
    {             
        // Random ball rotation
        GameObject ball = Instantiate(billardBall, position, Random.rotation);
        ball.GetComponent<Renderer>().material.mainTexture = texture;
    }

    private Vector3 RandomizeCueBallPosition(Vector3 centerPosition)
    {
        // TODO: Create real random placement with overlap checks

        Vector3 randomPosition = new Vector3(
            Random.Range(centerPosition.x + 0.1f, centerPosition.x + 0.45f),
            0,
            Random.Range(centerPosition.z + 0.1f, centerPosition.z + 0.45f)
        );
        return randomPosition;
    }

    private void Awake()
    {
        Vector3 centerPosition = new Vector3(0, 1.5f, 0); // start position of the first ball.
        float ballDiameter = 0.05715f;
        float ballRadius = ballDiameter / 2;
        Vector3 spacing = new Vector3(ballDiameter, ballDiameter, ballDiameter);
        // Bottom layer (9 balls in a 3x3 pattern)
        for (int x = -1; x <= 1; x++)
        {
            for (int z = -1; z <= 1; z++)
            {
                Vector3 positionOffset = new Vector3(x * spacing.x, 0, z * spacing.z);
                InstantiateBall(centerPosition + positionOffset, ballTextures[ballTextures.Count - 1]);
                ballTextures.RemoveAt(ballTextures.Count - 1); // Remove the texture after use
            }
        }

        // Middle layer (4 balls in a 2x2 pattern)
        for (int x = -1; x <= 0; x++)
        {
            for (int z = -1; z <= 0; z++)
            {
                Vector3 positionOffset = new Vector3(x * spacing.x + ballRadius, spacing.y-0.015f, z * spacing.z + ballRadius);
                InstantiateBall(centerPosition + positionOffset, ballTextures[ballTextures.Count - 1]);
                ballTextures.RemoveAt(ballTextures.Count - 1);
            }
        }

        // Top layer (1 ball)
        Vector3 topPositionOffset = new Vector3(0, 2 * spacing.y-0.03f,0);
        InstantiateBall(centerPosition + topPositionOffset, ballTextures[ballTextures.Count - 1]);
        ballTextures.RemoveAt(ballTextures.Count - 1);

        // The 15th ball below the 9 balls
        Vector3 belowPositionOffset = new Vector3(0, -spacing.y, 0); // Move down one diameter length
        InstantiateBall(centerPosition + belowPositionOffset, ballTextures[ballTextures.Count - 1]);

        // Randomize Cue ball position and instantiate it
        Vector3 cueBallPosition = new Vector3(0.3f, centerPosition.y, 0.3f);
        Instantiate(cueBall, cueBallPosition, Random.rotation);
    }
}
