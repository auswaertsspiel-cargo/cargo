using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class CueStick : MonoBehaviour
{
    [SerializeField] private GameObject cue;
    private Rigidbody cueStickRigidbody;

    private bool isRightTriggerDown = false;
    private bool isLeftTriggerDown = false;
    private bool isRightControllerInside = false;

    private Transform trackingSpace;
    private void Awake()
    {
        cueStickRigidbody = cue.GetComponent<Rigidbody>();
        trackingSpace = GameObject.Find("OVRPlayerController/OVRCameraRig/TrackingSpace").transform;

        if (trackingSpace == null)
        {
            Debug.LogError("Tracking space not found. Make sure your OVRCameraRig is set up correctly.");
        }
   
    }

    private void Update()
    {
        OVRInput.Update();

        isRightTriggerDown = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
        isLeftTriggerDown = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
    }

    private void FixedUpdate()
    {
        OVRInput.FixedUpdate();

        if (isRightTriggerDown && isRightControllerInside)
        {
            Vector3 rightControllerPosition = trackingSpace.TransformPoint(OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch));

            transform.position = rightControllerPosition;
            
            cueStickRigidbody.MovePosition(rightControllerPosition);

            if (isLeftTriggerDown)
            {
                Vector3 leftControllerPosition = trackingSpace.TransformPoint(OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch));
                AlignCueStick(leftControllerPosition);
            }
        }
    }



    private void AlignCueStick(Vector3 targetPosition)
    {
        Vector3 directionToTarget = (targetPosition - transform.position).normalized;
        Quaternion toTargetRotation = Quaternion.LookRotation(directionToTarget, Vector3.up);
        Quaternion correctedRotation = toTargetRotation * Quaternion.Euler(90f, 0f, 0f);

        transform.rotation = correctedRotation;
        cueStickRigidbody.MoveRotation(correctedRotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("RightController")) { 
            isRightControllerInside = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("RightController")) 
        {
            isRightControllerInside = false;
        }
    }
}
